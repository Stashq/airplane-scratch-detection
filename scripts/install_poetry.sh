#!/bin/bash

python -m venv .venv
.venv/bin/pip install -U pip setuptools
.venv/bin/pip install poetry
