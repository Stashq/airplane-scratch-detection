#!/bin/bash

APP_NAME=aircraft-scratch-detection

if [[ "$(docker images -q $APP_NAME 2> /dev/null)" == "" ]]; then
    echo -e "====== BUILDING IMAGE ======\n"
    docker build . -t $APP_NAME
    echo -e "\n"
fi

echo -e "\t====== RUNNING APP IN DOCKER ======\n"
docker run -it $APP_NAME $@
