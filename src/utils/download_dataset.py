from pathlib import Path
from typing import Literal

import requests

url_dict = {
    "mnist": "https://datahub.io/machine-learning/mnist_784/r/mnist_784.csv"
}


def download_dataset(
    path: Path,
    name: Literal["mnist"] | None = None,
    url: str | None = None,
    skip_if_alerady_exists: bool = True,
) -> None:
    if url is None and name is not None:
        assert name in url_dict, "Unknown dataset name."
        url = url_dict[name]
    assert url is not None, '"name" or "url" has to be not None.'

    if not (path.is_file() and skip_if_alerady_exists):
        req = requests.get(url)
        with open(path, "wb") as file:
            file.write(req.content)
