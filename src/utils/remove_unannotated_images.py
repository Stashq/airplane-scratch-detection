from pathlib import Path

from lxml import etree


def remove_unannotated_images(
    cvat_xml: Path,
    image_dirs: list[Path],
    based_on_annotated: bool = False,
    verbose: bool = True,
) -> None:
    """Removes images from provided directories.
    Two strategies are allowed based on looking into xml file:
     - annotated - collect all annotated images and delete REST,
     - unannotated - collect all NOT annotated images and delete THEM.

    WARNING:
    If images from more than one dataset are mixed and names of images from
    different dataset in same directory don't overlap, it's safer to use
    "unannotated" strategy."""
    root = etree.parse(cvat_xml).getroot()
    image_names = _get_image_names(root, annotated=based_on_annotated)
    count = 0
    for dir in image_dirs:
        for file in dir.iterdir():
            if file.is_file() and (
                # file name IS NOT in annotated image name list
                (based_on_annotated and file.name not in image_names)
                or
                # file name IS in NOT annotated image name list
                (not based_on_annotated and file.name in image_names)
            ):
                if verbose:
                    print(file)
                file.unlink()
                count += 1
    if verbose:
        print(f"{count} files erased.")


def _get_image_names(root: etree._Element, annotated: bool) -> list[str]:
    image_names = []
    for image_tag in root.findall(".//image"):
        # add file if has any annotation
        if annotated and len(image_tag) > 0:
            image_names += [image_tag.get("name")]
        # add file if does NOT has any annotation
        elif not annotated and len(image_tag) == 0:
            image_names += [image_tag.get("name")]
    return image_names
