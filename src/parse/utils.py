from pathlib import Path

import yaml


def load_label_dict_from_yolo(yolo_dataset_file: Path) -> dict[str | int, int]:
    names = yaml.safe_load(yolo_dataset_file.read_text())["names"]
    # swap keys with values
    label_dict = {v: k for k, v in names.items()}
    return label_dict
