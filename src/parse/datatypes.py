from dataclasses import dataclass, field

POINT = tuple[int, int] | tuple[float, float]


@dataclass
class DamageShape:
    type: str
    label: str | int
    z_order: int
    points: list[POINT] = field(default_factory=lambda: [])


@dataclass
class ImageAnnotation:
    id: int
    name: str
    width: int
    height: int
    shapes: list[DamageShape] = field(default_factory=lambda: [])

    def set_shapes(self, shapes: list[DamageShape]) -> None:
        self.shapes = shapes
        self.shapes.sort(key=lambda x: x.z_order)
