from pathlib import Path
from typing import Literal

from lxml import etree

from src.parse.datatypes import POINT, DamageShape, ImageAnnotation


def _parse_points(points_str: str) -> list[POINT]:
    points: list[POINT] = []
    for point_str in points_str.split(";"):
        values = point_str.split(",")
        assert len(values) == 2, f'Wrong point notation "{point_str}"'
        x, y = values
        points += [(float(x), float(y))]
    return points


def _parse_shape(
    shape_tag: etree._Element, shape_type: Literal["polygon", "box"] = "polygon"
) -> DamageShape:
    if shape_type == "box":
        points_str = "{0},{1};{2},{1};{2},{3};{0},{3}".format(
            shape_tag.get("xtl"),
            shape_tag.get("ytl"),
            shape_tag.get("xbr"),
            shape_tag.get("ybr"),
        )
    elif shape_type == "polygon":
        points_str = shape_tag.get("points")
    points = _parse_points(points_str)
    shape = DamageShape(
        type=shape_type,
        label=shape_tag.get("label"),
        points=points,
        z_order=int(shape_tag.get("z_order")),
    )
    return shape


def _parse_shapes(image_tag: etree._Element) -> list[DamageShape]:
    shapes = []
    for shape_type in ["polygon", "box"]:
        for shape_tag in image_tag.iter(shape_type):
            shapes += [_parse_shape(shape_tag, "polygon")]
    return shapes


def get_image_tag(root: etree._Element, image_name: str) -> etree._Element:
    image_name_attr = f".//image[@name='{image_name}']"
    image_iterator = root.iterfind(image_name_attr)

    image_tag = next(image_iterator, None)
    assert image_tag is not None, f'Image "{image_name}" not found in xml.'
    assert next(image_iterator, None) is None, (
        "Expected single record file per every image in xml, "
        f'but for "{image_name}" got at least 2.'
    )

    return image_tag


def parse_image_tag(root: etree._Element, image_name: str) -> ImageAnnotation:
    image_tag = get_image_tag(root, image_name)
    image = ImageAnnotation(
        id=int(image_tag.get("id")),
        name=image_tag.get("name"),
        width=int(image_tag.get("width")),
        height=int(image_tag.get("height")),
    )
    image.set_shapes(_parse_shapes(image_tag))
    return image


def parse_xml(cvat_xml: Path, image_names: list[str]) -> list[ImageAnnotation]:
    root = etree.parse(cvat_xml).getroot()
    annotations = []
    for image_name in image_names:
        annotations += [parse_image_tag(root, image_name)]
    return annotations
