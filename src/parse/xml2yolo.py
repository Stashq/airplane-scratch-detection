from pathlib import Path

from tqdm import tqdm

from src.parse.datatypes import POINT, ImageAnnotation
from src.parse.parse_xml import parse_xml


def assert_points_normalized(points: list[POINT]) -> None:
    def assert_val(val: float) -> None:
        assert 0 <= val <= 1, (
            "All points coordinates should be between 0 and 1."
            f" Received value: {val}"
        )

    for x, y in points:
        assert_val(x)
        assert_val(y)


def _normalize_points(image_anno: ImageAnnotation) -> ImageAnnotation:
    for shape in image_anno.shapes:
        for i in range(0, len(shape.points)):
            x, y = shape.points[i]
            shape.points[i] = (x / image_anno.width, y / image_anno.height)
        assert_points_normalized(shape.points)
    return image_anno


def _translate_labels(
    image_anno: ImageAnnotation, label_dict: dict[str | int, int]
) -> ImageAnnotation:
    for shape in image_anno.shapes:
        shape.label = label_dict[shape.label]
    return image_anno


def _save_as_yolo_file(
    image_annotation: ImageAnnotation,
    output_dir: Path,
    label_dict: dict[str | int, int] | None = None,
) -> None:
    output_dir.mkdir(parents=True, exist_ok=True)
    image_annotation = _normalize_points(image_annotation)
    if label_dict is not None:
        image_annotation = _translate_labels(image_annotation, label_dict)

    output_path = output_dir / (image_annotation.name.split(".")[0] + ".txt")

    def join_tuples(point: POINT) -> str:
        x, y = point
        return f"{x} {y}"

    with open(output_path, "w") as file:
        for shape in image_annotation.shapes:
            points_str = list(map(join_tuples, shape.points))
            line = str(shape.label) + " " + " ".join(points_str)
            file.write(line)


def produce_yolo_files(
    image_dir: Path,
    output_dir: Path,
    subdirs_relative_path: list[str],
    cvat_xml: Path,
    label_dict: dict[str | int, int] | None = None,
) -> None:
    output_dir.mkdir(parents=True, exist_ok=True)
    for subdir in subdirs_relative_path:
        path = image_dir / subdir
        image_names = [file.name for file in path.iterdir() if file.is_file()]
        annotations = parse_xml(cvat_xml=cvat_xml, image_names=image_names)
        for image_annotation in tqdm(annotations, desc="Writing contours:"):
            _save_as_yolo_file(
                image_annotation=image_annotation,
                output_dir=output_dir / subdir,
                label_dict=label_dict,
            )
