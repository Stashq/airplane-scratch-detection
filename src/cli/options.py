from pathlib import Path

import click

image_dir_option = click.option(
    "--image-dir",
    required=True,
    type=click.Path(
        file_okay=False,
        dir_okay=True,
        writable=False,
        readable=True,
        path_type=Path,
    ),
    default=Path("data/2023-03-07/images/"),
    show_default=True,
    help="Images root directory.",
)


subdirs_option = click.option(
    "--subdirs",
    required=True,
    type=click.Path(
        file_okay=False,
        dir_okay=True,
        writable=False,
        readable=True,
        path_type=Path,
    ),
    multiple=True,
    default=["train", "val"],
    show_default=True,
    help='Subdirectories relative to "--image-dir".',
)


cvat_xml_option = click.option(
    "--cvat-xml",
    required=True,
    type=click.Path(
        file_okay=True,
        dir_okay=False,
        writable=False,
        readable=True,
        path_type=Path,
        exists=True,
    ),
    default=Path("data/2023-03-07/annotations.xml"),
    show_default=True,
    help="CVAT annotation xml file.",
)


output_dir_option = click.option(
    "--output-dir",
    required=True,
    type=click.Path(
        file_okay=False,
        dir_okay=True,
        writable=False,
        readable=True,
        path_type=Path,
    ),
    default=Path("data/2023-03-07/labels"),
    show_default=True,
    help="Directory for output masks files.",
)
