from pathlib import Path

import click

from src.cli.options import (
    cvat_xml_option,
    image_dir_option,
    output_dir_option,
    subdirs_option,
)
from src.parse.utils import load_label_dict_from_yolo
from src.parse.xml2yolo import produce_yolo_files


@click.command()
@image_dir_option
@output_dir_option
@subdirs_option
@cvat_xml_option
@click.option(
    "--yolo-dataset-file",
    type=click.Path(
        file_okay=True,
        dir_okay=False,
        writable=False,
        readable=True,
        path_type=Path,
    ),
    default=Path("data/2023-03-07/yolo_data.yaml"),
    show_default=True,
    help="Yolo dataset yaml file.",
)
def main(
    image_dir: Path,
    output_dir: Path,
    subdirs: list[str],
    cvat_xml: Path,
    yolo_dataset_file: Path,
) -> None:
    """Produce yolo annotation files from cvat xml."""
    label_dict = load_label_dict_from_yolo(yolo_dataset_file)
    produce_yolo_files(
        image_dir=image_dir,
        subdirs_relative_path=subdirs,
        cvat_xml=cvat_xml,
        output_dir=output_dir,
        label_dict=label_dict,
    )


if __name__ == "__main__":
    main()
