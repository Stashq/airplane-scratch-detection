from pathlib import Path

import click

from src.cli.options import cvat_xml_option, image_dir_option, subdirs_option
from src.utils.remove_unannotated_images import remove_unannotated_images


@click.command()
@image_dir_option
@subdirs_option
@cvat_xml_option
@click.option(
    "--annotated",
    is_flag=True,
    show_default=True,
    default=False,
    help="If to remove all images others than annotated.",
)
def main(
    image_dir: Path, subdirs: list[str], cvat_xml: Path, annotated: bool
) -> None:
    """Removes images that are not annotated.
    By default removes images that are in cvat xml file but don't have
    any annotated shapes (safer option).
    If --annotated flag is used then preserves only images tagged as annotated
    and erases all others (can erase files that are not mentioned in xml file).
    """
    image_dirs = [image_dir / subdir for subdir in subdirs]
    remove_unannotated_images(
        cvat_xml=cvat_xml,
        image_dirs=image_dirs,
        based_on_annotated=annotated,
    )


if __name__ == "__main__":
    main()
