from ultralytics import YOLO

# Load the model.
model = YOLO("yolov8n.pt")

# Training.
results = model.train(
    data="./data/coco128.yaml",
    imgsz=640,
    epochs=3,
    batch=8,
    name="yolov8n_custom",
)
