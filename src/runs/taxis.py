from datetime import datetime
from typing import Any

import torch
from dotenv import load_dotenv

import wandb
from src.nn.data.taxis import TaxisDataModule


def taxis_run() -> None:
    taxis_data_module = TaxisDataModule()
    taxis_data_module.setup("predict")
    data_loader = taxis_data_module.predict_dataloader()
    model = torch.hub.load(
        "mateuszbuda/brain-segmentation-pytorch",
        "unet",
        in_channels=3,
        out_channels=1,
        init_features=32,
        pretrained=True,
    )

    load_dotenv()
    wandb.init(
        entity="Stan",
        job_type="testing_code",
        project="aircraft-scratch-detection",
        name=datetime.now().strftime("%Y/%m/%d-%H:%M:%S"),
        tags=["taxis", "test", "dummy"],
    )

    images: list[Any] = ["real"]
    preds: list[Any] = ["pred"]
    for x, y in data_loader:
        pred = model(x)
    images += [wandb.Image(img) for img in y]
    preds += [wandb.Image(img) for img in pred]

    table = wandb.Table(
        data=[images, preds],
        columns=["type"] + [str(i) for i in range(len(images))],
    )

    wandb.log({"Real-Pred": table})
