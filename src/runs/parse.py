from pathlib import Path

from src.parse.utils import load_label_dict_from_yolo
from src.parse.xml2yolo import produce_yolo_files


def xml2yolo() -> None:
    label_dict = load_label_dict_from_yolo(
        Path("data/2023-03-07/yolo_data.yaml")
    )
    produce_yolo_files(
        image_dir=Path("data/2023-03-07/images/"),
        subdirs_relative_path=["train", "val"],
        cvat_xml=Path("data/2023-03-07/annotations.xml"),
        output_dir=Path("data/2023-03-07/labels"),
        label_dict=label_dict,
    )


if __name__ == "__main__":
    xml2yolo()
