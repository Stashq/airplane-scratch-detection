import abc
from typing import Any, Literal, Union

import pytorch_lightning as pl
import torch
from torch import Tensor

STEP_OUTPUT = Tensor | dict[str, Any]
EPOCH_OUTPUT = Union[
    list[Union[Tensor, dict[str, Any]]],
    list[list[Union[Tensor, dict[str, Any]]]],
]
BATCH_TYPE = Any


class BaseLightningModule(pl.LightningModule, abc.ABC):
    def __init__(self) -> None:
        super().__init__()

    def training_step(self, batch: BATCH_TYPE, batch_idx: int) -> STEP_OUTPUT:
        loss = self._shared_eval(batch, batch_idx)
        return loss

    def validation_step(self, batch: BATCH_TYPE, batch_idx: int) -> STEP_OUTPUT:
        loss = self._shared_eval(batch, batch_idx)
        return loss

    def test_step(self, batch: BATCH_TYPE, batch_idx: int) -> STEP_OUTPUT:
        loss = self._shared_eval(batch, batch_idx)
        return loss

    @abc.abstractmethod
    def _shared_eval(self, batch: BATCH_TYPE, batch_idx: int) -> STEP_OUTPUT:
        pass

    def training_epoch_end(self, outputs: EPOCH_OUTPUT) -> None:
        self._shared_epoch_end(outputs=outputs, stage="train")

    def validation_epoch_end(self, outputs: EPOCH_OUTPUT) -> None:
        self._shared_epoch_end(outputs=outputs, stage="val")

    def test_epoch_end(self, outputs: EPOCH_OUTPUT) -> None:
        self._shared_epoch_end(outputs=outputs, stage="test")

    def _shared_epoch_end(
        self, outputs: EPOCH_OUTPUT, stage: Literal["train", "val", "test"]
    ) -> None:
        losses = []
        for o in outputs:
            losses += [o["loss"] if isinstance(o, dict) else o]
        mean_loss = torch.stack(losses).mean()
        self.log(f"{stage}_loss", mean_loss, on_step=False, on_epoch=True)
