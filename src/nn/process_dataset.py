import pickle
from pathlib import Path

import torch
from pytorch_lightning import LightningDataModule, LightningModule
from torch import Tensor
from tqdm.auto import tqdm


def process_dataset(
    model: LightningModule,
    datamodule: LightningDataModule,
    save_path: Path | None = None,
) -> dict[str, list[Tensor]]:
    # enabling evaluation mode
    model.eval()
    # loading data into dm
    datamodule.setup("fit")
    datamodule.setup("test")

    # processing all examples
    preds = []
    ys = []
    for dataloader in [
        datamodule.train_dataloader(),
        datamodule.val_dataloader(),
        datamodule.test_dataloader(),
    ]:
        for x, y in tqdm(dataloader):
            with torch.no_grad():
                pred = model(x)
                preds += [pred]
            ys += [y]

    result = {"predictions": preds, "true": ys}
    # saving
    if save_path is not None:
        with open(save_path, "wb") as handle:
            pickle.dump(result, handle, protocol=pickle.HIGHEST_PROTOCOL)

    return result
