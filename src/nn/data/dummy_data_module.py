import numpy as np
import numpy.typing as npt
import pytorch_lightning as pl
from torch import Tensor
from torch.utils.data import DataLoader, Dataset
from torchvision import transforms


class DummyDataModule(pl.LightningDataModule):
    def __init__(self, img_dimentions: tuple[int, int, int]) -> None:
        super().__init__()
        self.img_dimentions = img_dimentions

    def setup(self, stage: str) -> None:
        if stage == "fit":
            dataset = XYDataset(X=[self._rand_img()], y=[-1])
            self.train = DataLoader(dataset)
            dataset = XYDataset(X=[self._rand_img()], y=[-2])
            self.val = DataLoader(dataset)

        if stage == "test":
            dataset = XYDataset(X=[self._rand_img()], y=[-3])
            self.test = DataLoader(dataset)

        if stage == "predict":
            dataset = XYDataset(X=[self._rand_img()], y=[-4])
            self.predict = DataLoader(dataset)

    def train_dataloader(self) -> DataLoader[tuple[Tensor, Tensor]]:
        return self.train

    def val_dataloader(self) -> DataLoader[tuple[Tensor, Tensor]]:
        return self.val

    def test_dataloader(self) -> DataLoader[tuple[Tensor, Tensor]]:
        return self.test

    def predict_dataloader(self) -> DataLoader[tuple[Tensor, Tensor]]:
        return self.predict

    def _rand_img(self) -> npt.NDArray[np.float32]:
        return np.random.randn(*self.img_dimentions).astype("float32")


Xy_type = tuple[Tensor, Tensor]
X_in_type = npt.NDArray[np.int8 | np.float32]


class XYDataset(Dataset[Xy_type]):
    def __init__(
        self,
        X: X_in_type | list[X_in_type],
        y: list[int],
        transform: transforms.Compose | None = None,
    ):
        assert len(X) == len(y), "X and y have to have same length."
        self.X = X
        self.y = Tensor(y)
        if transform is None:
            transform = transforms.Compose(
                [
                    transforms.ToTensor(),
                ]
            )
        self.transform = transform

    def __getitem__(self, index: int) -> Xy_type:
        X = self.transform(self.X[index])
        return X, self.y[index]

    def __len__(self) -> int:
        return len(self.X)
