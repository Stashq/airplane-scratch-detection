import os

import pytorch_lightning as pl
import torch
from PIL import Image
from torch import Tensor
from torch.utils.data import DataLoader, Dataset
from torchvision import transforms


class TaxisDataset(Dataset[tuple[Tensor, Tensor]]):
    def __init__(self, data_dir: str = "./data/nyc-taxis"):
        super().__init__()
        self.data_dir = data_dir
        self.imgs_names = [
            file[:-4] for file in os.listdir(f"{data_dir}/images")
        ]
        self.transform = transforms.Compose(
            [
                transforms.Resize((512, 512)),
                transforms.PILToTensor(),
                transforms.ConvertImageDtype(torch.float),
            ]
        )

    def __len__(
        self,
    ) -> int:
        return len(self.imgs_names)

    def __getitem__(self, index: int) -> tuple[Tensor, Tensor]:
        name = self.imgs_names[index]
        img = self.transform(
            Image.open(f"{self.data_dir}/images/{name}.jpg").convert("RGB")
        )
        mask = self.transform(
            Image.open(f"{self.data_dir}/masks/{name}.png").convert("L")
        )

        return img, mask


class TaxisDataModule(pl.LightningDataModule):
    def __init__(self, data_dir: str = "./data/nyc-taxis"):
        super().__init__()
        self.data_dir = data_dir

    def setup(self, stage: str) -> None:
        self.dataset = TaxisDataset(self.data_dir)

    def train_dataloader(self) -> DataLoader[tuple[Tensor, Tensor]]:
        return DataLoader(self.dataset, batch_size=32)

    def val_dataloader(self) -> DataLoader[tuple[Tensor, Tensor]]:
        return DataLoader(self.dataset, batch_size=32)

    def test_dataloader(self) -> DataLoader[tuple[Tensor, Tensor]]:
        return DataLoader(self.dataset, batch_size=32)

    def predict_dataloader(self) -> DataLoader[tuple[Tensor, Tensor]]:
        return DataLoader(self.dataset, batch_size=32)
