from pathlib import Path

import numpy as np
import numpy.typing as npt
import pandas as pd
import torch
from torch import Tensor
from torch.utils.data import Dataset
from torchvision import transforms

Xy_type = tuple[Tensor, Tensor]
X_in_type = npt.NDArray[np.int8 | np.float32]


class MNISTDataset(Dataset[Xy_type]):
    def __init__(
        self,
        X: X_in_type | list[X_in_type] | None = None,
        y: list[int] | None = None,
        path: Path | None = None,
        transform: transforms.Compose | None = None,
    ):
        super().__init__()
        self.data_path = path
        if path is not None:
            X, y = self._load_data(path)

        assert (
            X is not None and y is not None
        ), "Both X and y have to be defined."
        assert len(X) == len(y), "X and y have to have same length."
        self.X, self.y = X, torch.tensor(y)

        if transform is None:
            transform = transforms.Compose(
                [
                    transforms.ToTensor(),
                    transforms.Normalize((33.356,), (78.618,)),
                ]
            )
        self.transform = transform

    def __getitem__(self, index: int) -> Xy_type:
        X = self.transform(self.X[index])
        return X, self.y[index]

    def __len__(self) -> int:
        return len(self.X)

    def _load_data(
        self, path: Path
    ) -> tuple[npt.NDArray[np.float32], list[int]]:
        mnist = pd.read_csv(path)
        X = (
            mnist.drop(columns="class")
            .to_numpy()
            .reshape(-1, 1, 28, 28)
            .transpose(0, 2, 3, 1)  # ToTensor assumes (B, H, W, C) order
            .astype("float32")
        )
        y = mnist["class"].to_list()
        return X, y
