from pathlib import Path

import pytorch_lightning as pl
from torch import Tensor
from torch.utils.data import DataLoader, Subset
from torchvision import transforms

from src.nn.data.mnist_dataset import MNISTDataset
from src.utils.download_dataset import download_dataset


class MNISTDataModule(pl.LightningDataModule):
    def __init__(
        self,
        data_path: Path = Path("./data/mnist_784.csv"),
        batch_size: int = 32,
        download: bool = False,
    ):
        super().__init__()
        self.data_path = Path(data_path)
        self.batch_size = batch_size
        self.transform = transforms.Compose(
            [transforms.ToTensor(), transforms.Normalize((33.356,), (78.618,))]
        )
        self.download = download

    def prepare_data(self) -> None:
        # download
        if self.download and not self.data_path.is_file():
            download_dataset(self.data_path, name="mnist")

    def setup(self, stage: str) -> None:
        dataset = MNISTDataset(path=self.data_path)
        train = Subset(dataset, range(60000))
        val = Subset(dataset, range(60000, 65000))
        test = Subset(dataset, range(65000, 70000))

        # Assign train/val datasets for use in dataloaders
        if stage == "fit":
            self.mnist_train = DataLoader(train, batch_size=self.batch_size)
            self.mnist_val = DataLoader(val, batch_size=self.batch_size)

        # Assign test dataset for use in dataloader(s)
        if stage == "test":
            self.mnist_test = DataLoader(test, batch_size=self.batch_size)

        if stage == "predict":
            self.mnist_predict = DataLoader(test, batch_size=self.batch_size)

    def train_dataloader(self) -> DataLoader[tuple[Tensor, Tensor]]:
        return self.mnist_train

    def val_dataloader(self) -> DataLoader[tuple[Tensor, Tensor]]:
        return self.mnist_val

    def test_dataloader(self) -> DataLoader[tuple[Tensor, Tensor]]:
        return self.mnist_test

    def predict_dataloader(self) -> DataLoader[tuple[Tensor, Tensor]]:
        return self.mnist_predict
