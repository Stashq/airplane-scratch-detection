from typing import Any

import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.callbacks.callback import Callback
from pytorch_lightning.loggers.logger import Logger
from torch.utils.data import DataLoader


def train(
    model: pl.LightningModule,
    datamodule: pl.LightningDataModule | DataLoader[Any],
    max_epochs: int = 10,
    min_epochs: int = 3,
    logger: Logger | bool = False,
    overfit_batches: int = 0,
    callbacks: list[Callback] | None = [ModelCheckpoint(monitor="val_loss")],
    **trainer_kwargs: Any,
) -> pl.LightningModule:
    trainer = pl.Trainer(
        max_epochs=max_epochs,
        min_epochs=min_epochs,
        logger=logger,
        overfit_batches=overfit_batches,
        auto_lr_find=True,
        callbacks=callbacks,
        **trainer_kwargs,
    )
    trainer.fit(model, datamodule)
    return model
