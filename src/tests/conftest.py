import pytest
import pytorch_lightning as pl
from torch import nn

from src.nn.data.dummy_data_module import DummyDataModule

in_channels = 3
img_dimentions = (28, 28, in_channels)


@pytest.fixture(scope="session")
def dummy_model() -> nn.Module:
    model = nn.Sequential(nn.Conv2d(in_channels, 1, 3))
    return model


@pytest.fixture(scope="session")
def dummy_data_module() -> pl.LightningDataModule:
    return DummyDataModule(img_dimentions=img_dimentions)
