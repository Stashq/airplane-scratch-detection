from pytorch_lightning import LightningDataModule, LightningModule

from src.nn.process_dataset import process_dataset


def test_process_dataset(
    dummy_model: LightningModule, dummy_data_module: LightningDataModule
) -> None:
    process_dataset(dummy_model, dummy_data_module)
