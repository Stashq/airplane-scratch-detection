# Project template

Template for python data science project. Dedicated for running on ``Scientific Computation`` Leipzig cluster. It is based on [Piotr Rarus](https://github.com/piotr-rarus) [template repo](https://github.com/piotr-rarus/python-template).

## Quick start

### TLTR - cluster commands

If you're already familiar with this template and want to run it on cluster just copy and paste following commands:

```console
module load Python/3.10.4-GCCcore-11.3.0
./scripts/install_poetry.sh
source .venv/bin/activate
poetry install
poetry run pre-commit install
```

### Naming your app (optional)

Before you start writing your code, it's good to create a name of your app (default: `your-app`). To do this change following files:

- [pyproject.toml](pyproject.toml) - variable `name` in section [tool.poetry] and variable `your-app` in section [tool.poetry.scripts] (change variable name, not path),
- [scripts/run_in_docker.sh](scripts/run_in_docker.sh) - variable `APP_NAME`,
- [Makefile](Makefile) - rules with targets named `run` and `docker_build`.

### Setup project

#### `Scientific Computation` cluster

If you're using `Scientific Computation` cluster, to create local environment, download `poetry` and install packages and pre-commits you have to run few commands.
First load available python version (default 3.10.4). Remember to set it also in [pyproject.toml](pyproject.toml) (section `[tool.poetry.dependencies]`, variable
`python`). Then run `install_poetry.sh` script, activate `venv`, install dependencies and pre-commit. You can do it running following commands:

```console
module load Python/3.10.4-GCCcore-11.3.0
module load PyTorch/1.12.0-foss-2022a-CUDA-11.7.0
./scripts/install_poetry.sh
```

If `poetry` installation fails follow [this instruction](https://python-poetry.org/docs/#installing-with-the-official-installer).

##### Load modules

Check what modules are already on the cluster and load these you are going to use. Start by activating venv:

```console
source .venv/bin/activate
```

then e.i.:

```console
module load PyTorch
```

#### Local machine

To install `poetry` (python dependency management tool) run:

```console
curl -sSL https://install.python-poetry.org | python3 -
```

If `poetry` installation fails follow [this instruction](https://python-poetry.org/docs/#installing-with-the-official-installer).

##### Install and select right python version

To manage python versions use `pyenv`. To install i.e. python 3.10.4 run:

```console
pyenv install 3.10.4
```

If you wish you can set it as default version:

```console
pyenv global 3.10.4
```

or locally in your project directory:

```console
pyenv local 3.10.4
```

Now select python environment for poetry:

```console
poetry env use $(which python3.10)
```

Remember to set right python version in [pyproject.toml](pyproject.toml) (section `[tool.poetry.dependencies]`, variable
`python`).

#### Install dependencies

To install dependencies and pre-commit hooks first activate venv:

```console
source .venv/bin/activate
```

or if you installed poetry as global tool (not in cluster case):

```console
poetry shell
```

Then run:

```console
poetry install
poetry run pre-commit install
```

##### Shortcut

After first installation if you haven't defined any new package in section `[tool.poetry]` for list `packages`, you can just use:

```console
make install_dev
```

### Run app

Initial program is a simple console app `Hello World!` placed in `src/main.py`. Feel free to change it and try to keep it as an entry point. You can execute it with command:

```console
poetry run python src/main.py `arg1` ...
```

using `Make` (only default arguments):

```console
make run
```

or if you named your app in first step:

```console
poetry run `your-app-name` `arg1` ...
```

## Project structure

- `src` - application code.
  - `src/main.py` - entry point.
  - `src/tests` - tests. Sometimes it is good to create `test` directories directly in the packages instead of in separate folder. The choice is yours.
  - `src/utils` - repeatedly used functions (i.e. logging).
- `scripts` - additional scripts.
  - `scripts/run_in_docker.sh` - run dockerized application. For more info check [Dockerizing script](#dockerizing-script) section.
  - `scripts/poetry_setup.sh` - script installing poetry on `Scientific Computing` cloud machine.
- `data` - if you need any data to be present locally, store it here. It is recommended to store the data in the cloud and connect it with repo using `DVC`.
- `notebooks` - notebooks are particularly _meh_, but this is the directory to put them.
- `src` - here goes your project's code.
- `.codespell` - whitelist for project-related terms.
- `.coveragearc` - corage config config, usually you don't want to report coverage on CLI, tests and some expressions.
- `.flake8` - `flake8` config.
- `isort.cfg` - `isort` config.
- `Makefile` - tasks definitions, much simpler to call `make` than writing whole commands in the terminal.
- `mypy.ini` - `mypy` config, usually some of your dependencies won't be hinted so you gonna ignore them here.
- `poetry.lock` - compiled dependencies.
- `poetry.toml` - `poetry` config.
- `pyproject.toml` - repo config.

## Tools

When developing a project there's a need to automate some tedious stuff that helps you keep the repo clean and check it against common standards. These include managing the environment, dependencies, syntax, etc.

- [coverage](https://github.com/nedbat/coveragepy)
- [flake8](https://github.com/PyCQA/flake8)
- [isort](https://github.com/PyCQA/isort)
- [mypy](https://github.com/python/mypy)
- [(vulture)](https://github.com/jendrikseipp/vulture)
- [poetry](https://github.com/python-poetry/poetry)

## Makefile

Below are listed most important Makefile commands.
To start your work, you need to set up your local environment and hooks.

```console
make install_dev
```

To format code with `isort` and `black` use:

```console
make format
```

To build project use:

```console
make build
```

To run coverage tests use:

```console
make test_cov
```

This will create `coverage.xml`, `junit/test-results.xml` summury files and `htmlcov` directory. Open in browser `htmlcov/index.html` to see how tests covered code.

## Dockerizing script

The easiest way to run application using docker is to run following script, which accepts same arguments as application:

```console
./scripts/run_in_docker.sh
```

It will build (if not already built) image (default: `your-app`). Be aware that this script won't override the image. Remember to create and declare a volume directory if your app return the artifacts.

### Docker build and run

To build image by your own, run:

```console
docker build . -t `your-app-name`
```

or use Makefile command:

```console
make docker_build
```

Then you can run dockerized application. Remember to mount host directory if needed. Use interactive mode to see logs in your console.

```console
docker run -v $(pwd)/`local_dir`:`dir/in/container` -it `your-app-name`
```

## Weights and Biases

To setup W&B as local server run:

```console
docker run --rm -d -v wandb:/vol -p 8080:8080 --name wandb-local wandb/local
```

If you run W&B standalone version on your server, put credentials in `.env` file and load them using:

```python
from dotenv import load_dotenv
load_dotenv()
```

`WARNING`
Remember that all should be setup for your wandb server, not general `wandb.ai`.

## DVC

Use DVC (Data Version Control) to synchronize data and versioning it (DVC can be used also in MLOps purpose but we have already W&B for that).

### Init repo

To start using DVC type:

```console
dvc init
```

then add your data to dvc:

```console
dvc add data/any_file
```

and follow prompts.

### Setup connection

After you initialized DVC repository you have to create connection with remove server where the data should be stored. To do it read section in this [tutorial](https://dvc.org/doc/user-guide/data-management/remote-storage) that fits your cloud storage server type.

#### Connection with nextcloud

To connect to nextcloud first copy a webdav link which you can find in Files (left top corner) -> Files settings (left bottom corner) -> WebDAV`. Then change _https_ to _webdavs_ and run:

```console
dvc remote add -d remote_name webdavs://example.com/owncloud/remote.php/dav/$PATH_TO_DIR
```

PATH_TO_DIR is a path to directory in nextcloud you want to connect with data stored in DVC repository. Then pass your username and password using `--local` option:

```console
dvc remote modify --local remote_name user $MY_USERNAME
dvc remote modify --local remote_name password $MY_PASSWORD
```

You can find those configurations in `.dvc/config` and `.dvc/config.local` files.

### Push/pull changes

To pull changes from cloud storage just run:

```console
dvc pull
```

If you wish to save applied changes first run:

```console
dvc commit
```

and then push it to remote server:

```console
dvc push
```
